+++
title = "Demo Servers"
+++
## Demo servers
The demo servers are meant to showcase the latest developments of the LibreHealth project. Each project should add a link to their demo site through this page.

## lh-toolkit LTS 1.12.0 demo
LibreHealth Toolkit master demo is a deployment that is done by GitLab CI after every commit.

 - URL - https://toolkit.librehealth.io/1.12.x
 - Username: admin
 - Password: Hello@123

## lh-toolkit LTS 2.0.0 demo
LibreHealth Toolkit LTS 2.0.0 demo is a deployment that is done by GitLab CI after every commit.

 - URL - https://toolkit.librehealth.io/master
 - Username: admin
 - Password: Hello@123

## lh-ehr demo
LibreHealth EHR demo is based on the latest stable release

 - URL - https://ehr.librehealth.io
 - Username: admin
 - Pass Phrase: password
